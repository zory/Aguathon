"""
This file is part of aguathon.

aguathon is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

aguathon is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with aguathon.  If not,
see <https://www.gnu.org/licenses/>.
"""
import pandas as pd
import numpy as np
from aemet import get_aemet_data


def reshape_data(data_list, timestep=2, jump=1):
    data = data_list.apply(tuple, axis=1).apply(list)
    data_ret = []
    for i in range(0, timestep):
        entry = []
        for j in range(0, timestep, jump):
            if i - timestep + j < 0:
                entry.append(data.iloc[0])
            else:
                entry.append(data.iloc[i - timestep + j])
        entry.append(data.iloc[i])
        data_ret.append(entry)
    for i in range(timestep, len(data)):
        entry = []
        for j in range(0, timestep, jump):
            entry.append(data.iloc[i - timestep + j])
        entry.append(data.iloc[i])
        data_ret.append(entry)
    # data_ret = np.array(data_ret)
    data_ret = np.reshape(data_ret, (len(data_ret), len(data_ret[0])*len(data_ret[0][0]), 1))
    return data_ret


def get_data(file, aemet=True):
    data = pd.read_csv(file).ffill().fillna(0)

    # Extract month and hour
    date_col = pd.to_datetime(data["time"])
    data["month"] = date_col.dt.month
    data["hour"] = date_col.dt.hour
    data["year"] = date_col.dt.year
    if aemet:
        print("Downloading data from AEMET...")
        aemet_data = get_aemet_data(date_col)
        if aemet_data is not None:
            for col in aemet_data:
                data[col] = aemet_data[col]

    return data


if __name__ == "__main__":
    data = get_data("ENTRADA/datos.csv")
    # data.to_csv("ENTRADA/datos_parsed3.csv", index=False)
